#!/usr/bin/env python2.7

"""
Columbia's COMS W4111.001 Introduction to Databases
Example Webserver

To run locally:

    python server.py

Go to http://localhost:8111 in your browser.

A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""

import os
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from flask import Flask, request, render_template, g, redirect, Response, session
from flask_table import Table, Col
from datetime import datetime

import threading
import time
import urllib2
import json
import random

userid = -1

ORDER_DISCOUNT = 5

import threading
import time
import sys

app = Flask(__name__)
app.debug = True
app.threaded = True
app.config['SECRET_KEY'] = 'development key'

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)

#
# The following is a dummy URI that does not connect to a valid database. You will need to modify it to connect to your Part 2 database in order to use the data.
#
# XXX: The URI should be in the format of: 
#
#     postgresql://USER:PASSWORD@w4111a.eastus.cloudapp.azure.com/proj1part2
#
# For example, if you had username gravano and password foobar, then the following line would be:
#
#     DATABASEURI = "postgresql://gravano:foobar@w4111a.eastus.cloudapp.azure.com/proj1part2"
#
DATABASEURI = "postgresql://ty2346:w4111_tty_sz@w4111vm.eastus.cloudapp.azure.com/w4111"

#
# This line creates a database engine that knows how to connect to the URI above.
#
engine = create_engine(DATABASEURI)

#
# Example of running queries in your database
# Note that this will probably not work if you already have a table named 'test' in your database, containing meaningful data. This is only an example showing you how to run queries in your database using SQLAlchemy.
#
engine.execute("""CREATE TABLE IF NOT EXISTS test (
  id serial,
  name text
);""")
engine.execute("""INSERT INTO test(name) VALUES ('grace hopper'), ('alan turing'), ('ada lovelace');""")


@app.before_request
def before_request():
    """
    This function is run at the beginning of every web request
    (every time you enter an address in the web browser).
    We use it to setup a database connection that can be used throughout the request.

    The variable g is globally accessible.
    """
    try:
        g.conn = engine.connect()
    except:
        print "uh oh, problem connecting to database"
        import traceback;
        traceback.print_exc()
        g.conn = None


@app.teardown_request
def teardown_request(exception):
    """
    At the end of the web request, this makes sure to close the database connection.
    If you don't, the database could run out of memory!
    """
    try:
        g.conn.close()
    except Exception as e:
        pass


#
# @app.route is a decorator around index() that means:
#   run index() whenever the user tries to access the "/" path using a GET request
#
# If you wanted the user to go to, for example, localhost:8111/foobar/ with POST or GET then you could use:
#
#       @app.route("/foobar/", methods=["POST", "GET"])
#
# PROTIP: (the trailing / in the path is important)
# 
# see for routing: http://flask.pocoo.org/docs/0.10/quickstart/#routing
# see for decorators: http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
#
@app.route('/')
def index():
    """
    request is a special object that Flask provides to access web request information:

    request.method:   "GET" or "POST"
    request.form:     if the browser submitted a form, this contains the data in the form
    request.args:     dictionary of URL arguments, e.g., {a:1, b:2} for http://localhost?a=1&b=2

    See its API: http://flask.pocoo.org/docs/0.10/api/#incoming-request-data
    """

    # DEBUG: this is debugging code to see what request looks like
    print request.args

    #
    # example of a database query
    #
    cursor = g.conn.execute("SELECT Artist_Name FROM ArtistFrom")
    names = []
    for result in cursor:
        names.append(result['artist_name'])  # can also be accessed using result[0]
    cursor.close()

    #
    # Flask uses Jinja templates, which is an extension to HTML where you can
    # pass data to a template and dynamically generate HTML based on the data
    # (you can think of it as simple PHP)
    # documentation: https://realpython.com/blog/python/primer-on-jinja-templating/
    #
    # You can see an example template in templates/index.html
    #
    # context are the variables that are passed to the template.
    # for example, "data" key in the context variable defined below will be
    # accessible as a variable in index.html:
    #
    #     # will print: [u'grace hopper', u'alan turing', u'ada lovelace']
    #     <div>{{data}}</div>
    #
    #     # creates a <div> tag for each element in data
    #     # will print:
    #     #
    #     #   <div>grace hopper</div>
    #     #   <div>alan turing</div>
    #     #   <div>ada lovelace</div>
    #     #
    #     {% for n in data %}
    #     <div>{{n}}</div>
    #     {% endfor %}
    #
    context = dict(data=names, userid=userid)

    #
    # render_template looks in the templates/ folder for files.
    # for example, the below file reads template/index.html
    #
    return render_template("index.html", **context)


#
# This is an example of a different path.  You can see it at:
# 
#     localhost:8111/another
#
# Notice that the function name is another() rather than index()
# The functions for each app.route need to have different names
#


@app.route('/loginpage')
def loginPage():
    global userid
    context = dict(userid=userid)
    return render_template("login.html", **context)


# Example of adding new data to the database


@app.route('/register', methods=['POST'])
def register():
    global userid
    if request.method == 'POST':
        tmp = -1
        username = request.form['username']
        password = request.form['password']
        cursor = g.conn.execute("SELECT user_id FROM userowncontact WHERE user_name = %s;", username)
        for result in cursor:
            tmp = result['user_id']
        cursor.close()
        if tmp == -1:
            maxUserID = []
            cursor = g.conn.execute("SELECT MAX(user_id) FROM userowncontact;")
            for result in cursor:
                maxUserID.append(result[0])
            cursor.close()
            maxUserID = maxUserID[0]
            newUserID = maxUserID + 1

            g.conn.execute('INSERT INTO UserOwnContact VALUES (%s, %s, %s, NULL ,%s , NULL, NULL)', newUserID, newUserID, username, password)
            msg = 'You\'ve successfully registered!'
            context = dict(msg=msg, userid=userid)
            return render_template("login.html", **context)
        else:
            error = 'The username you typed is already used. Please choose another one.'
            context = dict(error=error, userid=userid)
            return render_template("login.html", **context)


@app.route('/login', methods=['GET', 'POST'])
def login():
    global userid
    if request.method == 'POST':
        userid = -1
        username = request.form['username']
        password = request.form['password']
        cursor = g.conn.execute("SELECT user_id FROM userowncontact WHERE user_name = %s and password = %s;", username, password)
        for result in cursor:
            userid = result['user_id']
        cursor.close()
        if userid == -1:
            error = 'Oops! We cannot find this combination of username and password in our database.'
            context = dict(error=error, userid=userid)
            return render_template("login.html", **context)
        else:
            return redirect('/userProfile')


@app.route('/songs', methods=['GET', 'POST'])
def songs():
    global userid

    # Find user name
    cursor = g.conn.execute("SELECT user_name FROM userowncontact WHERE user_id = %s;", userid)
    userNames = []
    for result in cursor:
        userNames.append(result[0])
    user = userNames[0]

    # A variable indicating if user have successfully rated a song
    songRated = 0

    if request.method == 'GET':

        uLikeSong = request.args.get('likeSong')

        if uLikeSong != None:
            # Add song to user's favorite

            # First find the user_id, contact_id and song_mbid
            cursor = g.conn.execute("SELECT user_id, contact_id FROM userowncontact WHERE user_id = %s;", userid)
            # userInfo[0]['userID'] stores user_id, userInfo[0]['contactID'] stores contact_id
            userInfo = []
            for result in cursor:
                tempDict = dict(userID=result['user_id'], contactID=result['contact_id'])
                userInfo.append(tempDict)
            cursor.close()

            cursor = g.conn.execute("SELECT song_mbid FROM singsong WHERE title = %s;", uLikeSong)
            songMBID = []
            for result in cursor:
                songMBID.append(result[0])
            cursor.close()

            # Then check if user have already added current song to his/her favorite
            cursor = g.conn.execute("SELECT L.song_mbid "
                                    "FROM likesong L, userowncontact U "
                                    "WHERE L.user_id = U.user_id and U.user_id = %s;", userid)
            favoriteSongIDs = []
            for result in cursor:
                favoriteSongIDs.append(result[0])
            cursor.close()

            # If user haven't added current song to favorite, add it, else do nothing
            if songMBID[0] not in favoriteSongIDs:
                cursor = g.conn.execute("INSERT INTO likesong VALUES (%s, %s, %s);", userInfo[0]['userID'],
                                        userInfo[0]['contactID'], songMBID[0])
                cursor.close()

            context = dict(user=user, userid=userid, uLikeSong=uLikeSong)


        else:
            # Default present all songs to user
            cursor = g.conn.execute("SELECT S.song_mbid, A.artist_name, S.title, S.duration "
                                    "FROM singsong S LEFT JOIN artistfrom A ON S.artist_mbid = A.artist_mbid;")
            allSongs = []
            for result in cursor:
                tempDict = dict(songID=result['song_mbid'], artistName=result['artist_name'], title=result['title'],
                                duration=result['duration'])
                allSongs.append(tempDict)
            cursor.close()

            context = dict(userID=userid, user=user, allSongs=allSongs, uLikeSong=uLikeSong, songRated=songRated)

        return render_template('songs.html', **context)

    elif request.method == 'POST':
        userRating = request.form.get('rating')
        if userRating == '':
            userRating = None
        songID = request.form.get('songID')

        # Find the id of songs that user have rated
        ratedSongIDs = []
        cursor = g.conn.execute("SELECT R.song_mbid "
                                "FROM ratingforsong R, writerating W "
                                "WHERE R.rating_id = W.rating_id AND W.user_id = %s;", userid)
        for result in cursor:
            ratedSongIDs.append(result[0])
        cursor.close()

        if songID in ratedSongIDs and userRating is not None:
            # Update rating
            cursor = g.conn.execute("UPDATE writerating "
                                    "SET score = %s;", userRating)
            cursor.close()
            songRated = 1
        elif userRating is not None:
            # Find current max rating_id
            maxRaingID = []
            cursor = g.conn.execute("SELECT MAX(rating_id) FROM writerating;")
            for result in cursor:
                maxRaingID.append(result[0])
            cursor.close()
            maxRaingID = maxRaingID[0]
            toInsertID = maxRaingID + 1

            # Insert rating
            cursor = g.conn.execute("INSERT INTO writerating VALUES(%s, %s, %s, %s);", toInsertID, userid, userid,
                                    userRating)
            cursor.close()
            cursor = g.conn.execute("INSERT INTO ratingforsong VALUES(%s, %s)", toInsertID, songID)
            cursor.close()
            songRated = 1
        else:
            songRated = 2

        context = dict(songRated=songRated)
        return render_template('songs.html', **context)


@app.route('/mainPage', methods=['GET', 'POST'])
def mainPage():
    global userid
    if request.method == 'GET':
        artistfrom = []

        # Find user name
        cursor = g.conn.execute("SELECT user_name FROM userowncontact WHERE user_id = %s;", userid)
        userNames = []
        for result in cursor:
            userNames.append(result[0])
        user = userNames[0]
        uarea = request.args.get('area')
        ugender = request.args.get('gender')
        utype = request.args.get('type')

        ulike = request.args.get('like')
        uchecksong = request.args.get('checksong')
        ucheckconcert = request.args.get('checkconcert')
        usearch = request.args.get('search')

        # If user likes some artist, add him to his favorite artists
        if ulike != None:
            # Firstly find user_id, contact_id and artist_mbid
            cursor = g.conn.execute("SELECT user_id, contact_id FROM userowncontact WHERE user_id = %s;", userid)
            userInfo = []
            for result in cursor:
                tempDict = dict(userID=result['user_id'], contactID=result['contact_id'])
                userInfo.append(tempDict)
            cursor.close()

            cursor = g.conn.execute("SELECT artist_mbid FROM artistfrom WHERE artist_name = %s;", ulike)
            artistMBID = []
            for result in cursor:
                artistMBID.append(result[0])
            cursor.close()

            # Then check if user have already added current artist to his/her favorite
            cursor = g.conn.execute("SELECT L.artist_mbid "
                                    "FROM likeartist L, userowncontact U "
                                    "WHERE L.user_id = U.user_id and U.user_id = %s;", userid)
            favoriteAritstIDs = []
            for result in cursor:
                favoriteAritstIDs.append(result[0])
            cursor.close()

            # If user haven't added current artist to favorite, add it
            if artistMBID[0] not in favoriteAritstIDs:
                cursor = g.conn.execute("INSERT INTO likeartist VALUES (%s, %s, %s);", userInfo[0]['userID'],
                                        userInfo[0]['contactID'], artistMBID[0])
                cursor.close()
            context = dict(user=user, userid=userid, like=ulike)

        # Check the songs of an artist
        elif uchecksong != None:
            # Select the songs of the given artist from database
            cursor = g.conn.execute("SELECT S.title, S.duration "
                                    "FROM singsong S, artistfrom A "
                                    "WHERE S.artist_mbid = A.artist_mbid and A.artist_name = %s;", uchecksong)
            artistSongs = []
            for result in cursor:
                tempDict = dict(title=result['title'], duration=result['duration'])
                artistSongs.append(tempDict)
            if len(artistSongs) < 1:
                artistSongs = "Empty"
            context = dict(user=user, userid=userid, artistSongs=artistSongs)

        # Check the concerts of an artist
        elif ucheckconcert != None:
            # Select the concerts of the given artist from database
            cursor = g.conn.execute("SELECT A.artist_name, H.concertdate, H.location "
                                    "FROM artistfrom A, holdconcert H "
                                    "WHERE A.artist_mbid = H.artist_mbid AND A.artist_name = %s;", ucheckconcert)
            artistConcerts = []
            for result in cursor:
                tempDict = dict(artist=result['artist_name'], concertdate=result['concertdate'],
                                location=result['location'])
                artistConcerts.append(tempDict)
            if len(artistConcerts) < 1:
                artistConcerts = "Empty"
            context = dict(user=user, userid=userid, artistConcerts=artistConcerts)

        else:
            # Find all areas
            allAreas = []
            cursor = g.conn.execute("SELECT area_mbid, area_name FROM area;")
            for result in cursor:
                tempDict = dict(areaID=result['area_mbid'], areaName=result['area_name'])
                allAreas.append(tempDict)
            cursor.close()

            if uarea == None:
                cursor = g.conn.execute("SELECT * FROM artistFrom A1, area A2 WHERE A1.area_mbid = A2.area_mbid;")
            else:
                # Find corresponding area id given area name
                cursor = g.conn.execute("SELECT area_mbid FROM area WHERE area_name = %s;", uarea)
                areaIDs = []
                for result in cursor:
                    areaIDs.append(result[0])
                cursor.close()
                area_mbid = areaIDs[0]

                # Select artists given the conditions
                if usearch != "":
                    cursor = g.conn.execute("SELECT * FROM artistFrom A1, area A2 WHERE A1.gender = %s AND A1.type = %s AND A1.area_mbid = %s AND A1.area_mbid = A2.area_mbid AND A1.artist_name LIKE %s;",
                    ugender, utype, area_mbid, ('%'+usearch+'%'))
                else:
                    cursor = g.conn.execute("SELECT * FROM artistFrom A1, area A2 WHERE A1.gender = %s AND A1.type = %s AND A1.area_mbid = %s AND A1.area_mbid = A2.area_mbid;",
                    ugender, utype, area_mbid)

            for result in cursor:
                tmp = dict(artist_name=result['artist_name'], gender=result['gender'], startdate=result['startdate'],
                           enddate=result['enddate'], type=result['type'], area=result['area_name'])
                artistfrom.append(tmp)
            cursor.close()

            context = dict(user=user, userid=userid, artistfrom=artistfrom, allAreas=allAreas)

    return render_template('mainPage.html', **context)


@app.route('/userProfile')
def userProfile():
    global userid
    user = 'db'

    # Select user name of current user
    queryName = text("SELECT user_name FROM userowncontact WHERE user_id = :userid;")
    cursor = g.conn.execute(queryName, userid=userid)
    userName = []
    for result in cursor:
        userName.append(result[0])
    cursor.close()

    # Select facebook account of current user
    queryFacebook = text("SELECT facebook FROM userowncontact WHERE user_id = :userid;")
    cursor = g.conn.execute(queryFacebook, userid=userid)
    userFacebook = []
    for result in cursor:
        userFacebook.append(result[0])
    cursor.close()

    # Select telephone number of current user
    queryPhonenumber = text("SELECT phonenumber FROM userowncontact WHERE user_id = :userid;")
    cursor = g.conn.execute(queryPhonenumber, userid=userid)
    userPhonenumber = []
    for result in cursor:
        userPhonenumber.append(result[0])
    cursor.close()

    # Select songs user likes from database
    querySong = text("SELECT S.title "
                     "FROM singsong S, userowncontact U, likesong L "
                     "WHERE L.user_id = U.user_id AND S.song_mbid = L.song_mbid and U.user_id = :userid;")
    cursor = g.conn.execute(querySong, userid=userid)
    songs = []
    for result in cursor:
        songs.append(result[0])  # can also be accessed using result[0]
    cursor.close()

    # Select artist user likes from database
    queryArtist = text("SELECT A.artist_name "
                       "FROM artistfrom A, userowncontact U, likeartist L "
                       "WHERE L.user_id = U.user_id AND A.artist_mbid = L.artist_mbid and U.user_id = :userid;")
    cursor = g.conn.execute(queryArtist, userid=userid)
    artistnames = []
    for result in cursor:
        artistnames.append(result['artist_name'])  # can also be accessed using result[0]
    cursor.close()

    # Select the concerts of the artist user likes
    concerts = []
    for artist in artistnames:
        cursor = g.conn.execute("SELECT A.artist_name, H.concertdate, H.location "
                                "FROM artistfrom A, holdconcert H "
                                "WHERE A.artist_mbid = H.artist_mbid AND A.artist_name = %s;", artist)
        for result in cursor:
            tempDict = dict(artistName=result['artist_name'],
                            concertDate=result['concertdate'], location=result['location'])
            concerts.append(tempDict)
        cursor.close()

    # Make recommendations for current user
    recommendations = []
    allSongsOfFavoriteArtists = []
    for artist in artistnames:
        cursor = g.conn.execute("SELECT S.title "
                                "FROM artistfrom A, singsong S "
                                "WHERE A.artist_mbid = S.artist_mbid AND A.artist_name = %s;", artist)
        for result in cursor:
            allSongsOfFavoriteArtists.append(result['title'])
        cursor.close()
    for song in allSongsOfFavoriteArtists:
        if song not in songs:
            recommendations.append(song)
    recommendations = recommendations[:10]

    # Find the ratings of current user
    userRatings = []
    cursor = g.conn.execute("SELECT S.title, W.score "
                            "FROM singsong S, writerating W, ratingforsong R "
                            "WHERE S.song_mbid = R.song_mbid AND R.rating_id = W.rating_id AND W.user_id = %s;", userid)
    for result in cursor:
        tempDict = dict(title=result['title'], score=result['score'])
        userRatings.append(tempDict)
    cursor.close()

    context = dict(songs=songs, user=user, artistnames=artistnames, userid=userid, facebook=userFacebook[0],
                   phonenumber=userPhonenumber[0], username=userName[0], concerts=concerts,
                   recommendations=recommendations, userRatings=userRatings)
    return render_template('profile.html', **context)


@app.route('/searchuser', methods=['GET', 'POST'])
def search_user():
    global userid

    # Flag
    searchResults = False
    isGetRequest = False
    users = []

    if request.method == 'POST':
        # Extract values from requst
        facebook = request.form['facebook']

        # Seach database
        cursor = g.conn.execute("SELECT * "
                                "FROM userowncontact "
                                "WHERE facebook = %s;", facebook)
        for result in cursor:
            tempDict = dict(userName = result['user_name'], facebook = result['facebook'], phonenumber = result['phonenumber'])
            users.append(tempDict)
        cursor.close()

        if len(users) > 0:
            searchResults = True

        context = dict(searchResults = searchResults, users = users, isGetRequest = isGetRequest)

    else:
        isGetRequest = True
        context = dict(searchResults = searchResults, users = users, isGetRequest = isGetRequest)

    return render_template('searchUser.html', **context)



@app.route('/updateprofile', methods=['GET', 'POST'])
def update_profile():
    global userid

    # A variable indicating if user have updated his/her profile
    profileUpdated = False

    if request.method == 'POST':
        # Extract values from request
        userName = request.form['username']
        email = request.form['email']
        password = request.form['password']
        facebook = request.form['facebook']
        phonenumber = request.form['phonenumber']

        # Update database
        cursor = g.conn.execute("UPDATE userowncontact "
                                "SET user_name = %s, email = %s, password = %s, facebook = %s, phonenumber = %s "
                                "WHERE user_id = %s;", userName, email, password, facebook, phonenumber, userid)
        cursor.close()
        profileUpdated = True

    context = dict(profileUpdated=profileUpdated)

    return render_template('updateProfile.html', **context)


@app.route('/addartist', methods=['GET', 'POST'])
def add_artist():
    global userid

    # A variable indicating if user have successfully added artist
    artistAdded = False

    if request.method == 'POST':
        # Extract values from request
        artistName = request.form['artistname']

        gender = request.form['gender']
        if gender == "":
            gender = None

        areaId = request.form['area']
        if areaId != "":
            areaId = int(areaId)
        else:
            areaId = None

        type = request.form['type']
        if type == "":
            type = None

        startDate = request.form['startdate']
        if startDate == "":
            startDate = None

        endDate = request.form['enddate']
        if endDate == "":
            endDate = None

        # Find the artist_mbid of the artist to be inserted
        cursor = g.conn.execute("SELECT MAX(artist_mbid) FROM artistfrom;")
        maxArtistID = []
        for result in cursor:
            maxArtistID.append(result[0])
        cursor.close()
        maxArtistID = maxArtistID[0]
        toInsertId = maxArtistID + 1

        # Insert into database
        cursor = g.conn.execute("INSERT INTO artistfrom VALUES(%s, %s, %s, %s, %s, %s, '', %s)",
                                toInsertId, areaId, startDate, endDate, gender, type, artistName)
        cursor.close()

        artistAdded = True

    context = dict(artistAdded=artistAdded)

    return render_template('addArtist.html', **context)


@app.route('/addsong', methods=['GET', 'POST'])
def add_song():
    global userid

    # Flag
    songAdded = False

    if request.method == 'POST':
        # Extract values from request
        title = request.form['title']
        duration = request.form['duration']

        # Find the song_mbid of the song to be inserted
        cursor = g.conn.execute("SELECT MAX(song_mbid) FROM singsong;")
        maxSongID = []
        for result in cursor:
            maxSongID.append(result[0])
        cursor.close()
        maxSongID = maxSongID[0]
        toInsertId = maxSongID + 1

        # Insert to database
        cursor = g.conn.execute("INSERT INTO singsong VALUES(%s, null, %s, %s);", toInsertId, title, duration)
        cursor.close()
        songAdded = True

    context = dict(songAdded=songAdded)
    return render_template('addSong.html', **context)


@app.route('/signout')
def signout():
    global userid
    userid = -1
    return redirect('/')


if __name__ == "__main__":
    import click


    @click.command()
    @click.option('--debug', is_flag=True)
    @click.option('--threaded', is_flag=True)
    @click.argument('HOST', default='0.0.0.0')
    @click.argument('PORT', default=8111, type=int)
    def run(debug, threaded, host, port):
        """
        This function handles command line parameters.
        Run the server using:

            python server.py

        Show the help text using:

            python server.py --help

        """

        HOST, PORT = host, port
        print "running on %s:%d" % (HOST, PORT)
        app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


    run()
